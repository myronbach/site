<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create admin
        $admin = App\User::create([
            'name' => 'admin',
            'email' => 'admin@test.com',
            'password' => bcrypt('secret'),
            'api_token' => 'JQHl6mxRegY9K75MyZNCJrKO6irYp8SV1ofVYrhHXFYPWL7C68yCda9LkUIY',
            'remember_token' => str_random(10),
        ]);
    }
}
