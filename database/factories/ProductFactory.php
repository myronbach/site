<?php

use Faker\Generator as Faker;

$fakerProd = \Faker\Factory::create();
$fakerProd->addProvider(new \Bezhanov\Faker\Provider\Device($fakerProd));

$factory->define(App\Product::class, function (Faker $faker) use ($fakerProd) {
    return [
        'title' => $fakerProd->deviceModelName,
        'price' => $faker->numberBetween($min = 1000, $max = 9000),
        'count' => $faker->numberBetween($min = 1, $max = 10),
        'created_at' => $faker->dateTimeBetween($startDate = '-3 years', $endDate = 'now')
    ];
});
