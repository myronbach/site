<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function scopeSort($query, $sort)
    {
        $sorts = explode(',', $sort);
        foreach($sorts as $sort){
            $sortDir = starts_with($sort, '-') ? 'desc' : 'asc';
            $sortCol = ltrim($sort, '-');
            if($sort) {
                $query->orderBy($sortCol, $sortDir);
            }
        }
        return $query;
    }

    public function scopeFilter($query, $filters)
    {
        foreach($filters as $field=>$value){
            //$field = $key;
            $condition = '=';
            //$value = $val;
            if(is_array($value)){
                foreach($value as $key=>$val){
                    if($key == 'gte') $condition = '>=';
                    if($key == 'lte') $condition = '<=';
                    $query->where($field, $condition, $val);
                }
            } else if(!empty($value)){
                $query->where($field, $condition, $value);
            }
        }
        return $query;
    }

    /**
     * filters product for GET request
     * @param $query
     * @param $filters
     */
    /*public function scopeFilterProducts($query, $filters)
    {
        $filters = explode(',', $filters);
        foreach($filters as $filter){
            list($criterias, $value) =  explode(':', $filter);
            $condition = '=';
            $criterias = explode('|', $criterias );
            $field = $criterias[0];
            if($field == 'search'){
                $query->where('title', 'like', '%'.$value.'%');
            } else {
                if(in_array('ge', $criterias)) $condition = '>=';
                if(in_array('le', $criterias)) $condition = '<=';
                $query->where($field, $condition, $value);
            }

        }
    }*/
}
