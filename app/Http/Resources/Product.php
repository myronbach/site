<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
          'id' => $this->id,
            'title' => $this->title,
            'price' => $this->price,
            'count' => $this->count,
            'created_at' => $this->created_at->format('Y-m-d'),
        ];
    }
}
