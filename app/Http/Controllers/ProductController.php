<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Resources\ProductCollection;

class ProductController extends Controller
{
    protected $limit = 30;
    public function index(Request $request)
    {

        $query = Product::query();

        if($request->has('filter')){
            $query->filter($request->input('filter'));
        }
        $query->sort( $request->input('sort',''));
        $limit = $request->input('limit', $this->limit);

        //$response = $query->limit($limit)->get();
        $response = $query->paginate($limit);
        return new ProductCollection($response);
    }

}
