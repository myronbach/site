<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    public function login(Request $request)
    {
        $credentials = $request->only(['email','password']);
        if(auth()->attempt($credentials)){
            $user = auth()->user();
            $user->api_token = str_random(60);
            $user->save();
            return response()->json($user);
        }
        return response()->json([
            'error' => 'Unauthenticated user',
            'code' => 401,
        ], 401);
    }

    public function logout()
    {
        if($user = auth()->user()){
            $user->api_token = null;
            $user->save();
            return response()->json(['message' => 'Successfully logged out']);
        }
        return response()->json([
            'error' => 'Unable to logout user',
            'code' => 401
        ], 401);
    }

    public function user()
    {
        return response()->json(auth()->user());
    }
}
