
######Headers
    Accept: application\json
    Content-type: application/json
######Authorization
    Type: Bearer Token
    Token: JQHl6mxRegY9K75MyZNCJrKO6irYp8SV1ofVYrhHXFYPWL7C68yCda9LkUIY     

__get all products__

*POST https://tranquil-chamber-52676.herokuapp.com/api/products*

######Body: JSON(application/json)

    {
        "sort": "created_at,title,price, count",
        "limit": int,
        "filter": {
            "count": int,
            "price": {
                "gte": int,
                "lte": int
            },
            "created_at": {
                "gte": "YYYY-mm-dd",
                "lte": "YYYY-mm-dd"
            }
        }
    }
  
 >Сортування списку - значення ключа "sort", за такими параметрами: title, price, count, created_at    
 >Якщо сортується за кількома параметрами, то вони вказуються в рядку, через кому. 
     
  >   * від меншого до більшого - вказується назва параметру ( "price" );
  >  * від більшого до меншого - спочатку назви ставиться "-" ( "-price" )</p>
     
> Фільтрування списку - значення ключа "filter", за такими параметрами: count, price ,created_at
  
  >   * параметр рівний заначенню - "param": value;
  >   * значення параметру в межах:

        "param": {
            "gte": value, // від
            "lte": value  // до
        }
      
  >  Всі параметри  та межі "filter" є опційними (не обов'язковими).
